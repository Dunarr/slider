/**
 * SliderOptions: Slider initialisation options model:
 *
 * margin : margin between slide (in px) default: 10
 * slidePerView : Slide displayed in a view (int) default: 1
 * slidePerScroll : slide swiped at each swipe (int) default: 1
 * transition : animation duration (in seconds) default: .5
 * autoPlay : autoplay speed (in second, set to 0 to disable autoplay) default: 0
 * infinite : enable infinite swipe (boolean) default: false
 * controls : enable next and previous arrows to controll swiper (boolean) default: false
 * pagination : enable pagination to controll swiper, may have some bugs with slidePerScroll (int) default: false
 * swipe : enable touch/mouse swipe to controll swiper (int) default: false
 * breakpoints : configuration for each screen size,
 *      for each, you must choose the minimum window size of application
 *      and the corresponding options (array of :{size: int, options: SliderOptions })  default: []
 *
 */
interface SliderOptions {
    autoPlay?: number,
    breakpoints?: { size: number, options: SliderOptions }[]
    controls?: boolean,
    infinite?: boolean,
    margin?: number,
    pagination?: boolean,
    slidePerScroll?: number,
    slidePerView?: number,
    swipe?: boolean,
    transition?: number,
}

/**
 * SliderOption: enum of slider options for development purpose
 */
enum SliderOption {
    autoPlay = "autoPlay",
    transition = "transition",
    margin = "margin",
    slidePerView = "slidePerView",
    slidePerScroll = "slidePerScroll",
    infinite = "infinite",
    controls = "controls",
    pagination = "pagination",
    swipe = "swipe",
    breakpoints = "breakpoints",
}

/**
 * SliderPagination : The pagination module for slider
 * accessible by Slider.pagination
 *
 *
 *  constructor:
 */
class SliderPagination {
    private _activeBullet?: HTMLElement;
    private _activeBulletIndex: number = 0;
    private _bullets: HTMLElement[] = [];
    private _el: HTMLElement;
    private _slider: Slider;
    /**
     * @var visible boolean: hide / show pagination
     * (use render to apply changes)
     * (it's better recommended to use slider options)
     */
    public visible: boolean = true;

    /**
     * WIP
     * @constructor
     * @param slider Slider: the linked slider
     * create a new pagination for a slider
     */
    constructor(slider: Slider) {
        this._slider = slider;
        const pagination = document.createElement("div");
        pagination.classList.add("slider__pagination");
        this._el = pagination;
        this._slider.addListener(this.setActive.bind(this))
    }

    /**
     * @method setActive
     * @param index
     * set the active bullet
     */
    public setActive(index: number) {
        index = Math.ceil(index) % this._slider.paginationSize;
        if (this._activeBulletIndex != index) {
            this._activeBulletIndex = index;
            this._activeBullet?.classList.remove("active");
        }
        this._activeBullet = this._bullets[index];
        this._activeBullet.classList.add("active")
    }

    /**
     * @method render
     * force pagination rendering
     * used in slider rendering
     */
    public render(): HTMLElement {
        this._el.innerHTML = "";
        if (this.visible) {
            this._bullets = [];
            for (let i = 0; i < this._slider.paginationSize; i++) {
                const bullet = document.createElement("div");
                bullet.addEventListener("click", () => {
                    this._slider.index = i;
                    this.setActive(i);
                });
                bullet.classList.add("slider__pagination__bullet");
                bullet.style.transition = `width ${this._slider.getOption(SliderOption.transition)}s`;
                this._bullets.push(bullet)
            }
            this.setActive(this._slider.index);

            this._bullets.forEach(bullet => {
                this._el.appendChild(bullet)
            });
        }
        return this._el
    }
}

/**
 * Slider: Main class of the library
 *
 *
 */
class Slider {
    /**
     * @var Initialised
     * check if Sliders are initialised, for rendering event listener
     */
    private static Initialised: boolean = false;
    /**
     * @var DefaultOptions
     * the defaults options for every new slider
     * can be overwritten to change default behavior (see SliderOptions)
     */
    public static DefaultOptions: SliderOptions = {
        transition: .5,
        margin: 10,
        slidePerView: 1,
        autoPlay: 0,
        slidePerScroll: 1,
        infinite: false,
        controls: false,
        pagination: false,
        swipe: false,
        breakpoints: []
    };
    /**
     * @var Instances
     * list of created sliders
     */
    public static Instances: Slider[] = [];
    /**
     * @var _arrowNext
     * @var _arrowPrev
     * control arrows elements
     */
    private readonly _arrowNext: HTMLElement;
    private readonly _arrowPrev: HTMLElement;

    /**
     * @var _autoPlay
     * interval id for autoplay
     */
    private _autoPlay?: number;

    /**
     * @var _el
     * slider root
     */
    private _el: HTMLElement;

    /**
     * @var _index
     * real active slide
     */
    private _index: number = 0;

    /**
     *  @var _listeners
     *  list of event listeners watching for sliding
     */
    private _listeners: ((value: number) => void)[] = [];

    /**
     *  @var _options
     *  slider option
     */
    private readonly _options: SliderOptions;

    /**
     *  @var _pagination
     *  slider pagination (see Slider)
     */
    private _pagination: SliderPagination;

    /**
     *  @var _slides
     *  original slides of the slider
     */
    private _slides: HTMLElement[] = [];

    /**
     *  @var _slidesCloneNext
     *  @var _slidesClonePrev
     *  slides clones for infinite scroll
     */
    private _slidesCloneNext: HTMLElement[] = [];
    private _slidesClonePrev: HTMLElement[] = [];

    /**
     *  @var _touchDownPos
     *  position where the finger/mouse touched the slider
     *
     *  @var _touchActualPos
     *  current finger/mouse position tracking
     *
     *  @var _touchDown
     *  wether the slider is mooved by swipe
     *
     */
    private _touchActualPos: number = 0;
    private _touchDown: boolean = false;
    private _touchDownPos: number = 0;

    /**
     * @var _track
     * tag containing the slides in the slider
     */
    private readonly _track: HTMLElement;


    /**
     * Slider constructor can be called to create a slider (it's is recommended to use Slidet.Init() to instantiate sliders)
     * @param el HTMLElement
     * root of your slider containing the slides
     *
     * @param options SliderOptions
     * options of the slider (see SliderOptions)
     */
    constructor(el: HTMLElement, options: SliderOptions = {}) {
        // Rendering watcher initialising
        if (!Slider.Initialised) {
            window.addEventListener("resize", Slider.RenderAll);
            Slider.Initialised = true;
        }

        // Add slider to static list
        Slider.Instances.push(this);

        // merge options with defaults
        this._options = {...Slider.DefaultOptions, ...options};

        // register slider root
        this._el = el;
        this.el.classList.add('slider');

        // Create slides track
        this._track = document.createElement('div');
        this.track.addEventListener("transitionend", this.resetTrackPos.bind(this));
        this.track.classList.add('slider__track');

        // Add slides to track and remove these from root element
        while (this.el.firstElementChild) {
            const child = this.el.firstElementChild;
            this.addSlide(<HTMLElement>child);
            this.el.removeChild(child)
        }

        // Create controls arrows
        this._arrowPrev = document.createElement("div");
        this._arrowPrev.classList.add("slider__arrow");
        this._arrowPrev.classList.add("slider__arrow--prev");
        this._arrowPrev.addEventListener("click", this.prev.bind(this));
        this.el.appendChild(this._arrowPrev);

        this._arrowNext = document.createElement("div");
        this._arrowNext.classList.add("slider__arrow");
        this._arrowNext.classList.add("slider__arrow--next");
        this._arrowNext.addEventListener("click", this.next.bind(this));
        this.el.appendChild(this._arrowNext);

        // Create pagination
        this._pagination = new SliderPagination(this);
        this.el.appendChild(this.pagination.render());

        // Register event listeners for swipe
        this.el.addEventListener('touchstart', this.initSlide.bind(this));
        this.el.addEventListener('mousedown', this.initSlide.bind(this));

        window.addEventListener('touchmove', this.doSlide.bind(this));
        window.addEventListener('mousemove', this.doSlide.bind(this));


        window.addEventListener('touchend', this.endSlide.bind(this));
        window.addEventListener('mouseup', this.endSlide.bind(this));


        // Render the slider
        this.el.appendChild(this.track);
        this.render()
    }

    get pagination(): SliderPagination {
        return this._pagination;
    }

    set pagination(value: SliderPagination) {
        this._pagination = value;
    }

    /**
     * @method paginationSize
     * return the number of pages in the slider
     */
    get paginationSize(): number {
        if (this.getOption(SliderOption.infinite)) {
            return Math.ceil(this.size / this.getOption(SliderOption.slidePerScroll));
        } else {
            return Math.ceil((this.size - this.getOption(SliderOption.slidePerView)) / this.getOption(SliderOption.slidePerScroll)) + 1;
        }
    }

    /**
     * @method size
     * return the base number of slides in the slider
     */
    get size() {
        return this._slides.length
    }

    /**
     * @method index
     * return the current page for pagination display
     */
    get index(): number {
        return ((this._index / this.getOption(SliderOption.slidePerScroll)) + this.paginationSize) % this.paginationSize;
    }

    /**
     * @method index
     * @param value number: the slider page to go
     * change the active slide and slide to it
     */
    set index(value: number) {
        // set the real index according to the page
        this._index = value * this.getOption(SliderOption.slidePerScroll);

        //check if the index is within range
        if (!this.getOption(SliderOption.infinite)) {
            if (this._index < 0) {
                this._index = 0
            }
            if (this._index > this.size - this.getOption(SliderOption.slidePerView)) {
                this._index = this.size - this.getOption(SliderOption.slidePerView)
            }
        }
        // render the slider
        this.moveTrack();

        // notify the listeners of the change
        this.notifyListeners()
    }

    /**
     * @method slides
     * return all the slides in the track
     */
    get slides(): HTMLElement[] {
        return [...this._slidesClonePrev, ...this._slides, ...this._slidesCloneNext]
    }

    get track(): HTMLElement {
        return this._track;
    }

    get el(): HTMLElement {
        return this._el;
    }

    /**
     * @method touchOffset
     * return the swiping offset
     */
    get touchOffset() {
        if (this._touchDown) {
            return( this._touchActualPos - this._touchDownPos)?( this._touchActualPos - this._touchDownPos):0
        } else {
            return 0
        }
    }

    /**
     * @method offset
     * return the offset of the track
     */
    get offset() {
        return `calc(-${100 / this.slides.length * (this._index + (this.getOption(SliderOption.infinite)?this.size:0))}% + ${this.getOption(SliderOption.margin) / 2 + this.touchOffset}px)`;
    }

    /**
     * @method Init
     * @param selector string: css selector of the slider(s) to create
     * @param options: option to add to the sliders (see SliderOptions)
     *
     * Slider initialiser, mass create slider based on a selector,
     * recommended entrypoint of the library
     *
     */
    static Init(selector: string, options?: SliderOptions) {
        const elements = document.querySelectorAll(selector);
        for (let i = 0; i < elements.length; i++) {
            const a = new Slider(<HTMLElement>elements[i], options)
        }
    }

    /**
     * @method RenderAll
     * rerender every registered slider, called on window resize
     */
    static RenderAll() {
        Slider.Instances.forEach(slider => {
            slider.render()
        })
    }

    /**
     * @method renderTrack
     * render the slides in the slider track for infinite or not
     * called on slider rendering
     */
    public renderTrack() {
        this.track.innerHTML = "";
        this.slides.forEach(slide => {
            this.track.appendChild(slide);
        })
    }

    /**
     * @method addListener
     * @param listener the callback you want to add
     * add a listener on slider slide event
     */
    public addListener(listener: (value: number) => void): void {
        this._listeners.push(listener)
    }

    /**
     * @method next
     * slide to next page
     */
    public next(): number {
        return ++this.index
    }

    /**
     * @method prev
     * slide to previous page
     */
    public prev(): number {
        return --this.index
    }


    /**
     * WIP
     * @method addSlide
     *
     * add a slide tlo the slider
     */
    public addSlide(slide: HTMLElement): number {
        const clone = <HTMLElement>slide.cloneNode(true);
        clone.classList.add("slider__element--copy");
        clone.classList.add('slider__element');
        this._slides.push(clone);
        let copy = <HTMLElement>clone.cloneNode(true);
        copy.classList.remove("slider__element--copy");
        this._slidesClonePrev.push(copy);
        copy = <HTMLElement>clone.cloneNode(true);
        this._slidesCloneNext.push(copy);


        return this._slides.indexOf(clone)
    }

    /**
     * WIP
     * @param slide int index of the slide to remove
     * @param slide HTMLElement the element to remove
     * remove a slide from the slider
     */
    public removeSlide(slide: number): void;

    public removeSlide(slide: HTMLElement): void;

    public removeSlide(slide: any) {
        let index: number;
        if (typeof slide === "number") {
            index = slide
        } else {
            index = this._slides.indexOf(slide)
        }
        this.track.removeChild(this.track.childNodes[index]);
        this._slides.slice(index, 1);
        this._slidesCloneNext.slice(index, 1);
        this._slidesClonePrev.slice(index, 1);
        this.index = this.index
    }


    /**
     * @method render
     * create the static style of the slider for the current window size, according to the slider configuration
     */
    public render() {
        this.renderTrack();

        if(this.getOption(SliderOption.infinite)) {
            this.el.classList.add('infinite')
        } else {
            this.el.classList.remove('infinite')
        }

        const slideMargin = this.getOption(SliderOption.margin);
        const slideCount = this.track.childElementCount;
        const trackSize = slideCount * 100 / this.getOption(SliderOption.slidePerView);

        // Set the size of the track and the slides
        this.track.style.width = `${trackSize}%`;
        this.track.style.transition = `transform ${this.getOption(SliderOption.transition)}s`;

        this.slides.forEach(slide => {
            slide.style.width = `calc(${100 / slideCount}% - ${slideMargin}px)`;
            slide.style.marginRight = `${slideMargin}px`
        });

        // verify the index with the new options
        this.index = this.index;

        // hide/show the controls
        if (this.getOption(SliderOption.controls)) {
            this._arrowPrev.classList.remove("hidden");
            this._arrowNext.classList.remove("hidden")
        } else {
            this._arrowPrev.classList.add("hidden");
            this._arrowNext.classList.add("hidden")
        }

        // reset autoplay
        if (this._autoPlay) {
            clearInterval(this._autoPlay);
            this._autoPlay = undefined;
        }
        if (this.getOption(SliderOption.autoPlay) > 0) {
            this._autoPlay = setInterval(this.next.bind(this), this.getOption(SliderOption.autoPlay) * 1000)
        }
        // hide/show the pagination
        this.pagination.visible = this.getOption(SliderOption.pagination);
        this.pagination.render()
    }

    /**
     * @method getOption
     * @param optionName SelectOption: the name of the option to get
     * get an option for the current window size
     */
    getOption(optionName: SliderOption): any {
        if (typeof this._options[optionName] === "undefined") {
            throw Error(`undefined option "${optionName}"`)
        }
        const optGroup: SliderOptions =
            [...(this._options.breakpoints ?this._options.breakpoints: []), {size: 0, options: this._options}]
                .filter(({size, options}) => size < window.innerWidth && typeof options[optionName] !== "undefined")
                .sort((a, b) => b.size - a.size)[0].options;
        return optGroup[optionName]
    }

    /**
     * @method moveTrack
     * @param transition boolean: use transition to move the track
     * move the track to the active slide
     */
    public moveTrack(transition: boolean = true) {

        if (!transition) {
            this.track.style.transition = "none";
        }

        this.track.style.transform = `translateX(${this.offset})`;

        if (!transition) {
            this.track.offsetHeight; // Fix F****ng Rendering bug;
            this.track.style.transition = `transform ${this.getOption(SliderOption.transition)}s`;
        }
    }

    /**
     * @method notifyListeners
     * notify the listeners of the page changement
     */
    private notifyListeners(): void {
        this._listeners.forEach(listener => listener(this.index))
    }

    /**
     * @method resetTrackPos
     * repositioning of the track for the infinite scroll
     */
    private resetTrackPos() {
        if (this.getOption(SliderOption.infinite)) {
            if (this._index <= 0) {
                this._index = (this._index +this.size) % this.size;
            }
            if (this._index >= this.size - this.getOption(SliderOption.slidePerView)) {
                this._index = this._index % this.size;
            }
            this.moveTrack(false)
        }
    }



    /**
     * @method initSlide
     * @param event
     * store base touch/click position
     */
    private initSlide(event: TouchEvent): void;

    private initSlide(event: MouseEvent): void;

    private initSlide(event: any) {
        if (this.getOption(SliderOption.swipe)) {
            this._touchDown = true;
            if (event.touches) {
                if (event.touches.length === 1) {
                    this._touchActualPos = event.changedTouches[0].clientX;
                    this._touchDownPos = event.changedTouches[0].clientX;
                }
            } else {
                this._touchActualPos = event.clientX;
                this._touchDownPos = event.clientX;
            }
        }
    }

    /**
     * @method doSlide
     * @param event
     * move the track on mouse/finger move
     */

    private doSlide(event: TouchEvent): void;

    private doSlide(event: MouseEvent): void;

    private doSlide(event: any) {
        if (this._touchDown) {
            if (event.touches) {
                this._touchActualPos = event.changedTouches[0].clientX;
            } else {
                this._touchActualPos = event.clientX;
            }
            this.moveTrack(false)
        }
    }

    /**
     * @method endSlide
     * @param event
     * move the track and change active slide according to swipe when the user release it
     */
    private endSlide(event: TouchEvent): void;

    private endSlide(event: MouseEvent): void;

    private endSlide(event: any) {
        if (this._touchDown) {
            if(typeof Math.round(this.touchOffset / this.slides[0].clientWidth / this.getOption(SliderOption.slidePerScroll)) === "number"){
                this.index -= Math.round(this.touchOffset / this.slides[0].clientWidth / this.getOption(SliderOption.slidePerScroll));
            }
            this._touchDown = false;
            this._touchDownPos = 0;
            this._touchActualPos = 0;
            this.moveTrack(true)
        }
    }
}
