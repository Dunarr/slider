Slider.Init('#carousel1',
  {
    slidePerView: 1,
    pagination: true,
    margin: 10,
    autoPlay: 2,
    swipe: true,
    breakpoints: [
      {
        size: 1300, options: {
          margin: 100,
          transition: .5,
          swipe: true,
          pagination: true,
          infinite: true,
        },
      },
      {
        size: 400, options: {
          slidePerView: 2,
          controls: true,
          autoPlay: 0
        },
      },
      {
        size: 900, options: {
          slidePerView: 3,
          swipe: false,
          slidePerScroll: 2,
          transition: 2,
          pagination: false,
        },
      },
    ],
  });
